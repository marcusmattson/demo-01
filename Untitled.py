Image.open('./data/tb6.jpeg')
## Team Thunderbird
# Show some statistics about dataset
df_fifa19.describe()

# Shape of dataset (it has 17790 row and 87 columns)
df_fifa19.shape

# Number of unique elements in dataset
df_fifa19.nunique()

# I check where there are NaN values
df_fifa19.isnull().any()

# What columns are in dataset?
df_fifa19.columns

# Correlation heatmap
plt.rcParams['figure.figsize']=(25,16)
hm=sns.heatmap(df_fifa19[['Age', 'Overall', 'Potential', 'Value', 'Wage',
                'Acceleration', 'Aggression', 'Agility', 'Balance', 'BallControl', 
                'Body Type','Composure', 'Crossing','Dribbling', 'FKAccuracy', 'Finishing', 
                'HeadingAccuracy', 'Interceptions','International Reputation',
                'Joined', 'Jumping', 'LongPassing', 'LongShots',
                'Marking', 'Penalties', 'Position', 'Positioning',
                'ShortPassing', 'ShotPower', 'Skill Moves', 'SlidingTackle',
                'SprintSpeed', 'Stamina', 'StandingTackle', 'Strength', 'Vision',
                'Volleys']].corr(), annot = True, linewidths=.5, cmap='Blues')
hm.set_title(label='Heatmap of dataset', fontsize=20)
hm;

# Scatter plot shows correlation between Acceleration and other chosen features
def make_scatter(df_fifa19):
    feats = ('Agility', 'Balance', 'Dribbling', 'SprintSpeed')
    
    for index, feat in enumerate(feats):
        plt.subplot(len(feats)/4+1, 4, index+1)
        ax = sns.regplot(x = 'Acceleration', y = feat, data = df_fifa19)

plt.figure(figsize = (20, 20))
plt.subplots_adjust(hspace = 0.4)

make_scatter(df_fifa19)

# Histogram: number of players's age
sns.set(style ="dark", palette="colorblind", color_codes=True)
x = df_fifa19.Age
plt.figure(figsize=(12,8))
ax = sns.distplot(x, bins = 58, kde = False, color='g')
ax.set_xlabel(xlabel="Player\'s age", fontsize=16)
ax.set_ylabel(ylabel='Number of players', fontsize=16)
ax.set_title(label='Histogram of players age', fontsize=20)
plt.show()

# The five eldest players
eldest = df_fifa19.sort_values('Age', ascending = False)[['Name', 'Nationality', 'Age']].head(5)
eldest.set_index('Name', inplace=True)
print(eldest)

# The five youngest players
youngest = df_fifa19.sort_values('Age', ascending = True)[['Name', 'Nationality', 'Age']].head(5)
youngest.set_index('Name', inplace=True)
print(youngest)

# Compare six clubs in relation to age
some_clubs = ('Juventus', 'Real Madrid', 'Paris Saint-Germain', 'FC Barcelona', 'Legia Warszawa', 'Manchester United')
df_club = df_fifa19.loc[df_fifa19['Club'].isin(some_clubs) & df_fifa19['Age']]

# fig, ax = plt.subplots()
fig.set_size_inches(20, 10)
ax = sns.violinplot(x="Club", y="Age", data=df_club);
ax.set_title(label='Distribution of age in some clubs', fontsize=20);