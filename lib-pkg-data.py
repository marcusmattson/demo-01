# Data manipulation
import numpy as np
import pandas as pd

# Data visualization
import matplotlib.pyplot as plt
import seaborn as sns

# Display propertice
pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 100)

# Date
import datetime

# Maps
import geopandas as gpd
import pycountry
from math import pi

# Add packages
# open terminal for session or use this code in the interactive interpreter
!pip3 install geopandas
!pip3 install pycountry

# Maps
import geopandas as gpd
import pycountry
from math import pi

# Display in Jupyter
from IPython.display import display, HTML

df_fifa19 = pd.read_csv("./data/data.csv")