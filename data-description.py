# Show the first five rows
df_fifa19.head()

# Show the info about dataset
df_fifa19.info()

# Show some statistics about dataset
df_fifa19.describe()

# Shape of dataset (it has 17790 row and 87 columns)
df_fifa19.shape

# Number of unique elements in dataset
df_fifa19.nunique()

# I check where there are NaN values
df_fifa19.isnull().any()

# What columns are in dataset?
df_fifa19.columns